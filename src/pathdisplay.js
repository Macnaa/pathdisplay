
function readSingleFile(e) {
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    drawPath(contents, file);
  };

  reader.readAsText(file);
}

function displayContents(contents) {
  var element = document.getElementById('file-content');
  element.textContent = contents;
}

function extractFromCSV(csv_data, delim){
  var strData = csv_data;
  var strDelimiter = (delim || ",");

  var objPattern = new RegExp(
    (
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

        "([^\"\\" + strDelimiter + "\\r\\n]*))"
    ),
    "gi"
    );

  var arrData = [[]];
  var arrMatches = null;

  while (arrMatches = objPattern.exec( strData )){

      var strMatchedDelimiter = arrMatches[ 1 ];

      if (
          strMatchedDelimiter.length &&
          strMatchedDelimiter !== strDelimiter
          ){

          arrData.push( [] );
      }

      var strMatchedValue;
      if (arrMatches[ 2 ]){

          strMatchedValue = arrMatches[ 2 ].replace(
              new RegExp( "\"\"", "g" ),
              "\""
              );

      } else {
          strMatchedValue = arrMatches[ 3 ];
      }
      arrData[ arrData.length - 1 ].push( strMatchedValue );
  }

  return( arrData );
}

function indexOfMax(array){
  if (array.length === 0){
    return -1;
  }

  var max = array[0];
  var maxIndex = 0;

  for (var i = 1; i < array.length; i++){
    if (array[i] > max){
      maxIndex = i;
      max = array[i];
    }
  }
  return maxIndex;
}

function newJSONFeature(dataPoints, currentAction, id){
  if (currentAction === null || dataPoints.length < 1) {
    return "";
  }

  var feature = '{\n' +
      '"type": "Feature",\n' +
      '"geometry": {\n' +
        '"type": "LineString",\n' +
        '"coordinates": [\n';

  for (var i = 0; i < dataPoints.length; i++){
    var coord = dataPoints[i];
    if (coord.length < 3){
      continue
    }
    feature += '[\n' +
      coord[2] + ',\n' +
      coord[1] + ' \n' +
          '],';
  }

  feature = feature.slice(0, -1);
  feature += "]},";
  feature += '"properties": {\n' +
        '"id": ' + id + ',\n' +
        '"action": "' + currentAction + '",\n' +
        '"timestamp": "' + dataPoints[0][0] + '",\n' +
        '"value": "0"\n' + '}}';

  return feature;
}


function arrayToJSON(data, id){
  var headers = data[0];

  var json_data = '{\n' +
  '"type": "FeatureCollection",\n'+
  '"features": [\n';
  var json_tail = "]}";

  var current_action = null;
  var currentDataPoints = [];

  for (var i = 1; i < data.length; i++){

    var row = data[i];
    if (row.length < 10){
      continue;
    }

    var action_dist = row.slice(3);
    var idx = indexOfMax(action_dist) + 3;

    if (current_action != headers[idx]){
      var newFeature = newJSONFeature(currentDataPoints, current_action, id);
      json_data += newFeature;

      if (current_action != null){
        json_data += ",\n";
      }

      current_action = headers[idx];
      if (currentDataPoints.length > 0){
        currentDataPoints = [currentDataPoints.slice(-1)[0], row.slice(0,3)];
      }else{
        currentDataPoints = [row.slice(0,3)];
      }
    } else {
      currentDataPoints.push(row.slice(0,3));
    }

  }

  json_data += newJSONFeature(currentDataPoints, current_action, id);

  return json_data + json_tail;

}

function convertToJSON(csv_file){
  var data = extractFromCSV(csv_file, ',');
  var json_data = arrayToJSON(data, fileCount++);
  //displayContents(json_data);
  return json_data;
}


function drawPath(csv_file, fileObject){

  var json_data = convertToJSON(csv_file);

  var movements = JSON.parse(json_data);
  var initCoords = movements.features[0].geometry.coordinates[0];

  map.panTo(new L.LatLng(initCoords[1], initCoords[0]));
  //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    //id: 'mapbox.satellite'
    //id: 'mapbox.streets'
  //}).addTo(map);


   geoJSONLayer = L.geoJson(movements, {
    style: function (feature) {
      var line_style;
      var actionColor = "black";
      var actionWeight = 2;
      var actionStroke = true;

      switch (feature.properties.action) {
        case 'Walking':
          actionColor = "red"
          break;

        case 'Running':
          actionColor = "blue"
          break;

        case 'Standing':
          actionColor = "yellow"
          break;

        case 'Crawling':
          actionColor = "green"
          break;

        case 'LyingSupine':
          actionColor = "cyan"
          break;

        case 'LyingProne':
          actionColor = "black"
          break;

        case 'Transition':
          actionColor = "white"
          break;
      }

      line_style = {
        stroke: actionStroke,
        color: actionColor,
        weight: actionWeight};


      return line_style
    },
    onEachFeature: function (feature, layer) {
        layer.bindPopup("id: " + feature.properties.id +"<br>"+
                        "action: " + feature.properties.action +"<br>"+
                          "timestamp: " + feature.properties.timestamp);
    }
  });

  //makeCheckbox(geoJSONLayer, fileObject)

  //geoJSONLayer.addTo(map);
  overlayMaps[fileObject.name] = geoJSONLayer;
  control_layer.remove(map);
  control_layer = L.control.layers(baseMaps, overlayMaps).addTo(map);
}


// Main Control Segment:

var satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  id: 'mapbox.satellite'});
var streets = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  id: 'mapbox.streets'});

var map = L.map('mapid', { center: [174.763232, -36.8728545],
                           zoom: 20,
                           layers: [satellite, streets]});

var baseMaps = {
  "Streets" : streets,
  "Satellite" : satellite
};

var overlayMaps = {};
var control_layer = L.control.layers(baseMaps, overlayMaps).addTo(map);
var fileCount = 0;

document.getElementById('file-input')
  .addEventListener('change', readSingleFile, false);

